FROM debian:testing-backports
RUN apt update
RUN apt install -y lua5.4 liblua5.4-dev luarocks
RUN luarocks --lua-version 5.4 install lunatest
RUN luarocks --lua-version 5.4 install luacheck
RUN apt install -y g++ cmake git
RUN luarocks install --server=https://luarocks.org/dev luaformatter
